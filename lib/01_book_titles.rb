class Book
  def title=(title)
    uncap_words = ['the', 'and', 'in', 'of', 'a', 'an']
    @title = title.split(' ').map.with_index do |el, i|
      uncap_words.include?(el) && i != 0 ? el : el.capitalize
    end.join(' ')
  end

  def title
    @title
  end
end
