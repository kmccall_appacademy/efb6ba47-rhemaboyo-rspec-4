class Dictionary
  attr_accessor :entries

  def initialize
    @entries = {}
  end

  def add(entry = {})
    entry = { entry => nil } if entry.is_a?(String)
    entry.each do |key, value|
      entries[key] = value
    end
  end

  def keywords
    entries.keys.sort
  end

  def include?(entry)
    entries.include?(entry)
  end

  def find(str)
    hash = {}
    entries.each do |key, value|
      hash[key] = value if key.include?(str)
    end
    hash
  end

  def printable
    arr = []
    keywords.each do |word|
      arr << "[#{word}] \"#{entries[word]}\""
    end
    arr.join("\n")
  end

end
