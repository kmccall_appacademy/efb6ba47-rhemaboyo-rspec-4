class Temperature
  attr_accessor :degrees
  def initialize(degrees = {})
    @degrees = degrees
  end

  def in_fahrenheit
    if degrees[:f]
      degrees[:f]
    else
      degrees[:c] * (9.0 / 5.0) + 32
    end
  end

  def in_celsius
    if degrees[:c]
      degrees[:c]
    else
      (degrees[:f] - 32) * (5.0 / 9.0)
    end
  end

  def self.from_celsius(temp)
    Temperature.new(c: temp)
  end

  def self.from_fahrenheit(temp)
    Temperature.new(f: temp)
  end


end

class Celsius < Temperature
  attr_accessor :temp

  def initialize(temp)
    @temp = temp
  end

  def in_celsius
    temp
  end

  def in_fahrenheit
    temp * (9.0 / 5.0) + 32
  end

end

class Fahrenheit < Temperature
  attr_accessor :temp

  def initialize(temp)
    @temp = temp
  end

  def in_celsius
    (temp - 32) * (5.0 / 9.0)
  end

  def in_fahrenheit
    temp
  end

end
